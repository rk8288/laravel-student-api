<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //We can declare $fillable with student columns 
    protected $fillable = ['name', 'date_of_birth', 'nationality', 'phone_number'];
}
