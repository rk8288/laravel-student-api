<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ImportBooks extends Model
{
    //Import Data
    public static function insertData($data)
    {     
      //Check book is already existed or not
      $value=DB::table('books')->where('id', $data['id'])->get();

      //If book is not existed insert into database
      if($value->count() == 0)
      {
        DB::table('books')->insert($data);
      }
      else//If book exists then we need to update the book
      {
      	DB::table('books')->where('id', $data['id'])->update($data);
      }
   }

   //Get books data
   public static function getBooksData()
   {
   	 $books_data = DB::table('books')->get();
   	 return $books_data;
   }
}
