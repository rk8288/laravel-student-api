<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class UserController extends Controller 
{
	public $successStatus = 200;
	/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login()
    { 
        //Validate email and password. Once credentials are validated token send to the responce
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')]))
        { 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else//If user not validated redirecting to 401 unauthorised user
        { 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

	/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        //Validating input request parameters
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);

        //If validation fail then we are redirecting to 401 with error responce
		if ($validator->fails()) 
		{ 
		    return response()->json(['error'=>$validator->errors()], 401);            
		}

		//If validation success we are creating the user and we are sending user details as responce
		$input = $request->all(); 
		        $input['password'] = bcrypt($input['password']); 
		        $user = User::create($input); 
		        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
		        $success['name'] =  $user->name;
		
		return response()->json(['success'=>$success], $this-> successStatus); 
    }

	/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        //Once user is authenticatied sending user details as responce
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 
}
