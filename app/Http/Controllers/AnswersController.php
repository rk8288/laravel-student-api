<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnswersController extends Controller
{
    //Function to genarate a string with given sequence
	function generateOutput(Request $request)
	{
		//Getting request parameters
		$a = $request->input('a');
		$b = $request->input('b');
		$c = $request->input('c');

		//validate given inputs
		if(!is_numeric($a))
		 return '$a should be an integer';
		else if(!is_numeric($b))
		 return '$b should be an integer';
		else if(!is_numeric($c))
		 return '$c should be an integer';

		//Alphabets Array
		$alphabets = array(0=>'a',1=>'b',2=>'c',3=>'d',4=>'e',5=>'f',6=>'g',7=>'h',8=>'i',9=>'j',10=>'k',11=>'l',12=>'m',13=>'n',14=>'o',15=>'p',16=>'q',17=>'r',18=>'s',19=>'t',20=>'u',21=>'v',22=>'w',23=>'x',24=>'y',25=>'z');
		//Initially pointer starts from 0
		$pointer = 0;
		$sequence = '';
		//Genarate the required string output 
		for($i=0; $i<=$c; $i=$i+$b)
		{
			//If series number divisiable by $a, We should replace number with the alphabet
			if($i%$a == 0)
			{
				$sequence .= $alphabets[$pointer];
				$pointer++;
			}
			else//If series number not divisable by $a then we add the number to output string
			{
				$sequence .= $i;
			}

			//If pointer is gratter than 25 then we need to initilize pointer again
			if($pointer > 25)
				$pointer =0;
		}
		
		echo  $sequence;
	}

	//Function to format the given array
	public function answer2(Request $request)
	{
		$input_array = json_decode(file_get_contents("php://input"), true);

		//Call the function to generate output string
		if(empty($input_array))
		{
			$inptu_array = Array(
							0 => Array('name' => 'Tom','age' => 32,
										 'key' => Array(0 => 'abc',1 => 'def',2 => 'efg')
										),
							1 => Array('name' => 'Jim','age' => 30,
										 'key' => Array(0 => 'abc',1 => 'def',2 => 'efg'),
										 'address' => Array(0 => Array('state' => 'CA','country' => 'US'),
															1 => Array('state' => 'NY','country' => 'US')
															)
										)
						);
		}		

		array_walk($input_array,  array('self','formatArray'));
	}

	//Function to genarate a string with given sequence
	public function formatArray($value, $key)
	{
		//To display user, name and age
		echo "User: ".$key."<br>";
		echo "&nbsp;&nbsp;tname: ".$value["name"]."<br>";
		echo "&nbsp;&nbsp;age: ".$value["age"];
		//If keys array exists then we need to format keys
		if(!empty($value["key"]))
		{
			echo "<br>&nbsp;&nbsp;key: <br>";
			foreach ($value["key"] as $user_key => $key_value) {
				echo "&nbsp;&nbsp;&nbsp;$user_key: ".$value["key"][$user_key]."<br>";
			}
		}
		//If address array exists then we need to format addresses
		if(!empty($value["address"]))
		{
			echo "&nbsp;&nbsp;address: <br>";
			foreach ($value["address"] as $address_key => $address_value) {
				echo "&nbsp;&nbsp;&nbsp;$address_key: <br>";
				foreach ($value["address"][$address_key] as $address_key2 => $address_value2) {
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$address_key2: ".$value["address"][$address_key][$address_key2]."<br>";
				}
			}
		}
	}
}
