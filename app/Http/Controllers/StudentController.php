<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    //Get all student data
    public function index()
    {
        return Student::all();
    }
 	
 	//Get a particular student data with student id
    public function show(Student $student)
    {
        return $student;
    }

    //Create a new student
    public function store(Request $request)
    {
        $student = Student::create($request->all());
        return response()->json($student, 201);
    }

    //Update a student with student id
    public function update(Request $request, Student $student)
    {
        $student->update($request->all());

        return $student;
    }

    //Delete a student with student id
    public function delete(Student $student)
    {
        $student->delete();

        return response()->json(['success'=>'Student deleted successfully'], 200);
    }
}
