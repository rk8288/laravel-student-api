<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\ImportBooks;
use Orchestra\Parser\Xml\Facade as XmlParser;

class ImportBooksController extends Controller
{
  //Books index page
  public function index()
  {
    return view('index');
  }

  //Import books xml
  public function uploadFile(Request $request)
  {
    if ($request->input('submit') == 'import_books' )
    {
      $file = $request->file('file');

      // File Details 
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $tempPath = $file->getRealPath();
      $fileSize = $file->getSize();
      $mimeType = $file->getMimeType();

      // Valid File Extensions
      $valid_extension = array("xml");

      // 2MB in Bytes
      $maxFileSize = 2097152; 

      // Check file extension
      if(in_array(strtolower($extension),$valid_extension))
      {
        // Check file size
        if($fileSize <= $maxFileSize)
        {
          // File upload location
          $location = 'uploads';

          // Upload file
          $file->move($location,$filename);

          // Import CSV to Database
          $filepath = public_path($location."/".$filename);

          // Reading file
          $xml_file_data = XmlParser::load($filepath);
          $books_data = $xml_file_data->parse([
            'books' => ['uses' => 'book[::id>id,author,title,genre,price,publish_date,description]'],
          ]);

          if(!empty($books_data['books']))
          {
            // Insert to MySQL database
            foreach ($books_data['books'] as $book) 
            {
              ImportBooks::insertData($book);
            }
          }
          
          Session::flash('success','Import Successful.');
        }
        else
        {
          Session::flash('fail','File too large. File must be less than 2MB.');
        }
      }
      else
      {
         Session::flash('fail','Invalid File Extension.');
      }

    }

    // Redirect to index
    return redirect()->action('ImportBooksController@index');
  }

  //Export books data into .csv file
  public function exportBooks(Request $request)
  {
    if ($request->input('submit') == 'export_books' )
    {
      //Get Books Data
      $books_data = ImportBooks::getBooksData();
      //CSV Row Header
      $array_key = array("Book ID","Author","Title","Genre","Price","Published Date","Description");
      $output = fopen("php://output",'w') or die("Can't open php://output");
      //CSV Headers
      header("Content-Type:application/csv"); 
      header("Content-Disposition:attachment;filename=books.csv"); 
      fputcsv($output, $array_key);
      //Write books data into CSV
      if ($books_data) 
      {
        foreach($books_data as $row_object) 
        {
          $row =(array)$row_object;
          fputcsv($output, $row);       
        }
      }
      //Close file reader
      fclose($output) or die("Can't close php://output");
    }
  }
}
