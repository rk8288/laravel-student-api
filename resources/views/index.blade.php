<!doctype html>
<html>
  <head>
    <title>Import Books</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
     <!-- Message -->
     @if(Session::has('success'))
      <div class="alert alert-success" style="margin-top: 15px;">
        <p >{{ Session::get('success') }}</p>
      </div>
     @endif
     <!-- Message -->
     @if(Session::has('fail'))
      <div class="alert alert-danger" style="margin-top: 15px;">
        <p >{{ Session::get('fail') }}</p>
      </div>
     @endif
     <div class="row">
        <div class="form-group col-md-6">
          <div class="col-md-4"></div>
          <label for="file"><strong>Import Books Data</strong></label>
           <!-- Import Books Form -->
           <form method='post' action='/uploadFile' enctype='multipart/form-data' >
             {{ csrf_field() }}
             <input type='file' name='file' required>
             <input type='submit' name='submit' value='import_books' class='btn btn-success'>
           </form>
         </div>
       </div>
       <div class="row">
        <div class="form-group col-md-4">
         <div class="col-md-4"></div>
           <label for="file"><strong>Export Books Data</strong></label>
          <!-- Export Books Form -->
          <form method='post' action='/exportBooks' enctype='multipart/form-data' >
            {{ csrf_field() }}
            <input type='submit' name='submit' value='export_books' class='btn btn-success'>
          </form>
        </div>
       </div>
      </div>
    </div>
  </body>
</html>