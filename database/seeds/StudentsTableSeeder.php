<?php

Use App\Student;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Student::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few student in our database:
        for ($i = 0; $i < 50; $i++) {
            Student::create([
                'name' => $faker->name,
                'date_of_birth' => $faker->dateTime,
                'nationality' => $faker->country,
                'phone_number' => $faker->phoneNumber,
            ]);
        }
    }
}
