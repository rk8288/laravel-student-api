<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Answers for the project questions routes
Route::post('answer1', 'AnswersController@generateOutput');
Route::post('answer2', 'AnswersController@answer2');

//User login / registration routes
Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

//Student API Routes
Route::group(['middleware' => 'auth:api'], function(){
	Route::get('students', 'StudentController@index');
	Route::get('students/{student}', 'StudentController@show');
	Route::post('students', 'StudentController@store');
	Route::put('students/{student}', 'StudentController@update');
	Route::delete('students/{student}', 'StudentController@delete');
});